#![feature(let_chains)]

use std::fs::File;
use std::io::{Write, Read};
use std::thread;
use std::sync::mpsc;

// Used to limit memory consumption
const FRAME_LIMIT: bool = true;

const WIDTH: usize = 1280;
const HEIGHT: usize = 720;
const BUFSIZE: usize = WIDTH*HEIGHT*3;

fn read_file(filename: &str) -> Result<Vec<u8>, String> {
    let mut file = File::open(filename)
        .map_err(|e| format!("read_file {filename}: {e}"))?;
    let mut bytes = vec![];
    file.read_to_end(&mut bytes)
            .map_err(|e| format!("read_file {filename}: {e}"))?;
    Ok(bytes)
}
fn decode_combine_write(bytes1: Vec<u8>, bytes2: Vec<u8>, filename: &str) -> Result<(), String> {
    // Spawn decoders
    let frame_limit = 100;
    let (tx_rgb1, rx_rgb1) = mpsc::channel();
    let (tx_rgb2, rx_rgb2) = mpsc::channel();
    let (tx_frame1, rx_frame1) = mpsc::channel();
    let (tx_frame2, rx_frame2) = mpsc::channel();
    let (tx_quit1, rx_quit1) = mpsc::channel();
    let (tx_quit2, rx_quit2) = mpsc::channel();
    thread::spawn(move || {
        let dec_config = openh264::decoder::DecoderConfig::new();

        #[cfg(debug_assertions)]
        let dec_config = {
            dec_config.debug(true)
        };

        let mut decoder1 = openh264::decoder::Decoder::with_config(dec_config)
            .map_err(|e| format!("decoder new 1: {e}"))
            .unwrap();

        let mut frame1 = 0usize;
        let mut rgb1 = vec![0; BUFSIZE];
        for packet1 in openh264::nal_units(&bytes1) {
            if let Ok(_) = rx_quit1.try_recv() {
                break;
            }

            if FRAME_LIMIT {
                if frame1 % frame_limit == 0 {
                    let _ = tx_frame1.send(frame1);
                    while let Ok(_frame2) = rx_frame2.try_recv() {}
                    if let Ok(_frame2) = rx_frame2.recv() {}
                }
            }

            // YUV 1
            let yuv1 = decoder1.decode(packet1)
                .map_err(|e| format!("decode 1: {e}"))
                .unwrap();
            if yuv1.dimension_rgb().0 == 0 || yuv1.dimension_rgb().1 == 0 {
                rgb1.fill(0);
            } else {
                yuv1.write_rgb8(&mut rgb1)
                    .map_err(|e| format!("write_rgb 1: {e}"))
                    .unwrap();
            }

            if let Err(m) = tx_rgb1.send(rgb1) {
                eprintln!("decode tx rgb1: {m}");
                break;
            }
            rgb1 = vec![0; BUFSIZE];

            frame1 += 1;
        }
    });
    thread::spawn(move || {
        let dec_config = openh264::decoder::DecoderConfig::new();

        #[cfg(debug_assertions)]
        let dec_config = {
            dec_config.debug(true)
        };

        let mut decoder2 = openh264::decoder::Decoder::with_config(dec_config)
            .map_err(|e| format!("decoder new 2: {e}"))
            .unwrap();

        let mut frame2 = 0usize;
        let mut rgb2 = vec![0; BUFSIZE];
        for packet2 in openh264::nal_units(&bytes2) {
            if let Ok(_) = rx_quit2.try_recv() {
                break;
            }

            if FRAME_LIMIT {
                if frame2 % frame_limit == 0 {
                    let _ = tx_frame2.send(frame2);
                    while let Ok(_frame1) = rx_frame1.try_recv() {}
                    if let Ok(_frame1) = rx_frame1.recv() {}
                }
            }

            // YUV 2
            let yuv2 = decoder2.decode(packet2)
                .map_err(|e| format!("decode 2: {e}"))
                .unwrap();
            if yuv2.dimension_rgb().0 == 0 || yuv2.dimension_rgb().1 == 0 {
                rgb2.fill(0);
            } else {
                yuv2.write_rgb8(&mut rgb2)
                    .map_err(|e| format!("write_rgb 2: {e}"))
                    .unwrap();
            }

            if let Err(m) = tx_rgb2.send(rgb2) {
                eprintln!("decode tx rgb2: {m}");
                break;
            }
            rgb2 = vec![0; BUFSIZE];

            frame2 += 1;
        }
    });

    // Spawn encoder and writer
    let filename = filename.to_string();
    let (tx_out, rx_out) = mpsc::channel::<Vec<u8>>();
    let (tx_quit3, rx_quit3) = mpsc::channel();
    let encode_thread = thread::spawn(move || {
        let enc_config = openh264::encoder::EncoderConfig::new(WIDTH as u32, HEIGHT as u32);
        
        #[cfg(debug_assertions)]
        let enc_config = {
            enc_config.debug(true)
        };
        
        let mut encoder = openh264::encoder::Encoder::with_config(enc_config)
            .map_err(|e| format!("encoder new: {e}"))
            .unwrap();
        
        let mut bytes_out = vec![];
        
        // Encode
        for rgb_out in rx_out {
            if let Ok(_) = rx_quit3.try_recv() {
                break;
            }

            let mut rgbyuv = openh264::formats::RBGYUVConverter::new(WIDTH, HEIGHT);
            rgbyuv.convert(&rgb_out);
            
            let bitstream = encoder.encode(&rgbyuv)
                .map_err(|e| format!("encode: {e}"))
                .unwrap();

            if filename == "-" {
                std::io::stdout().write(&bitstream.to_vec())
                    .map_err(|e| format!("write stdout: {e}"))
                    .unwrap();
            } else {
                bytes_out.extend(bitstream.to_vec());
            }
        }

        // Write
        if filename != "-" {
            let mut file = File::create(&filename)
                .map_err(|e| format!("write {filename}: {e}"))
                .unwrap();
            file.write(&bytes_out)
                .map_err(|e| format!("write {filename}: {e}"))
                .unwrap();
        }
    });

    // Decode, combine, send to encoder
    let mut frame = 0usize;
    let mut rgb_out = vec![0; BUFSIZE];
    while let Ok(rgb1) = rx_rgb1.recv()
        && let Ok(rgb2) = rx_rgb2.recv()
    {
        if frame % 100 == 0 {
            println!("frame={}", frame);
            std::io::stdout().flush()
                .map_err(|e| format!("flush: {e}"))?;
        }

        // RGB OUT
        let rgb1 = &rgb1[..BUFSIZE];
        let rgb2 = &rgb2[..BUFSIZE];
        let rgb3 = &mut rgb_out[..BUFSIZE];
        for (rgb3v, (rgb1v, rgb2v)) in rgb3.iter_mut()
            .zip(rgb1.iter()
                .zip(rgb2.iter()))
        {
            // ADD
            // *rgb3v = rgb1v.saturating_add(*rgb2v);

            // SUB
            // *rgb3v = rgb1v.saturating_sub(*rgb2v);
            // *rgb3v = rgb2v.saturating_sub(*rgb1v);
            
            // XOR
            // *rgb3v = rgb1v ^ rgb2v;

            // Burn
            // let target = *rgb2v as f32 / 255.0;
            // let blend = *rgb1v as f32 / 255.0;
            // if blend == 0.0 {
            //     *rgb3v = 0;
            // } else {
            //     *rgb3v = ((1.0 - ((1.0 - target) / blend)) * 255.0) as u8;
            // }

            // Dodge
            let target = *rgb2v as f32 / 255.0;
            let blend = *rgb1v as f32 / 255.0;
            if blend == 0.0 {
                *rgb3v = 0;
            } else {
                *rgb3v = ((target / (1.0 - blend)) * 255.0) as u8;
            }
        }

        tx_out.send(rgb_out)
            .map_err(|e| format!("encode tx: {e}"))?;
        rgb_out = vec![0; BUFSIZE];

        frame += 1;
    }

    // Send quits but ignore errors
    let _ = tx_quit1.send(());
    let _ = tx_quit3.send(());
    let _ = tx_quit2.send(());
    tx_out.send(rgb_out)
        .map_err(|e| format!("encode tx: {e}"))?;

    encode_thread.join()
        .map_err(|e| format!("encode join: {e:?}"))?;

    Ok(())
}

fn main() -> Result<(), String> {
    let mut args = std::env::args().skip(1);
    let filename1 = args.next()
        .ok_or_else(|| "missing filename1 arg".to_string())?;
    let filename2 = args.next()
        .ok_or_else(|| "missing filename2 arg".to_string())?;
    let filename_out = args.next()
        .ok_or_else(|| "missing filename_out arg".to_string())?;

    eprintln!("read_file1: {filename1}");
    let bytes1 = read_file(&filename1)?;
    eprintln!("read_file2: {filename2}");
    let bytes2 = read_file(&filename2)?;

    eprintln!("dcw: {filename_out}");
    decode_combine_write(bytes1, bytes2, &filename_out)?;

    Ok(())
}
