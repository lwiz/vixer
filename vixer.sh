#!/bin/bash

ffmpeg=$(which ffmpeg)
vixer="target/release/vixer"

filename1=$1
filename2=$2
filename_out=$3

cargo build --release

$ffmpeg -n -i $filename1 -vf "scale=1280:720,fps=30" ${filename1/mp4/h264}
$ffmpeg -n -i $filename2 -vf "scale=1280:720,fps=30" ${filename2/mp4/h264}

set -e

if [[ "$filename_out" == "-" ]]; then
    time $vixer ${filename1/mp4/h264} ${filename2/mp4/h264} - | ffmpeg -i - -f avi -r 30 - | vlc -
else
    time $vixer ${filename1/mp4/h264} ${filename2/mp4/h264} ${filename_out/mp4/h264}

    $ffmpeg -y -i ${filename_out/mp4/h264} -vf "fps=30" $filename_out
fi
